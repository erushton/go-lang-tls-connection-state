package main

import (
	"bufio"
	"crypto/x509"
	"encoding/hex"
	"fmt"
	"os"
	"sort"

	"net/http"
	"runtime"

	"github.com/zakjan/cert-chain-resolver/certUtil"
)

func main() {
	dumpConnectionState("https://gitlab.com/api/v4/")
	dumpConnectionState("https://letsencrypt.org/")
	return

	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		dumpConnectionState(scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		panic(err)
	}

}

func isSelfSigned(cert *x509.Certificate) bool {
	return cert.CheckSignatureFrom(cert) == nil
}

func isChainRootNode(cert *x509.Certificate) bool {
	if isSelfSigned(cert) {
		return true
	}
	return false
}

func dumpConnectionState(url string) {
	fmt.Println("URL", url, "with", runtime.Version())
	r, err := http.Head(url)
	if err != nil {
		panic(err)
	}

	fmt.Println("VerifiedChains len", len(r.TLS.VerifiedChains))

	// If there are multiple trusted chains, just play with the longest on the
	// assumption that this will give us the one that ends in a root node.
	// This is likely a false assumption in the general case but fine for this
	// example.
	chains := r.TLS.VerifiedChains
	sort.SliceStable(chains, func(i, j int) bool {
		return len(chains[i]) < len(chains[j])
	})

	// for i, verifiedChain := range r.TLS.VerifiedChains {
	i := 0
	verifiedChain := chains[len(chains)-1]
	fmt.Println("Chain #", i)

	// If last link in chain isn't a root node, fetch the rest

	c := verifiedChain[len(verifiedChain)-1]
	if !isChainRootNode(c) {
		verifiedChain, err = certUtil.FetchCertificateChain(c)
		if err != nil {
			fmt.Println(err)
			return
		}

		verifiedChain, err = certUtil.AddRootCA(verifiedChain)
		if err != nil {
			fmt.Println(err)
			return
		}
	}

	for j, certificate := range verifiedChain {
		signature := hex.EncodeToString(certificate.Signature)
		fmt.Println("[", j, "] =>", certificate.Subject.CommonName, signature)
	}
	// }
}
